/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package efit.biz.websockettest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateful;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Филипп
 */
@Stateful
@ServerEndpoint("/endpoint")
public class WSEndpoint {

    private final static Set<Session> SESSIONS = Collections.synchronizedSet(new HashSet<Session>());
    private final int connectionLimit = 2;

    @OnOpen
    public void connectionOpened(Session session) {
        try {
            if (SESSIONS.size() < connectionLimit) {
                SESSIONS.add(session);
                session.getBasicRemote().sendText(String.format("Время на сервере: %s", LocalDateTime.now().toString()));
            } else {
                session.getBasicRemote().sendText("дисконнект, пёс");
                connectionClosed(session);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @OnMessage
    public synchronized void processMessage(Session session, String message) {
        try {
            for (Session sess : SESSIONS) {
                if (sess.isOpen()) {
                    sess.getBasicRemote().sendText(message);
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @OnClose
    public void connectionClosed(Session session) {
        try {
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @OnError
    public void connectionError(Throwable throwable) {
        System.out.println("onError() invoked");
        throwable.printStackTrace();
    }

}
